import 'package:flutter/material.dart';
import 'package:flutter_utility/data/cars.dart';
import 'package:flutter_utility/widget/value_widget.dart';

class SortPage extends StatefulWidget {
  @override
  _SortPageState createState() => _SortPageState();
}

class _SortPageState extends State<SortPage> {
  @override
  Widget build(BuildContext context) {
    final sortedcars = cars..sort((a, b) => a.compareTo(b));
    return Center(
        child: ValuesWidget(title: "Sorted Cars", values: sortedcars));
  }
}
