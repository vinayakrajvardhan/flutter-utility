import 'package:flutter/material.dart';

import 'package:flutter_utility/data/cars.dart';
import 'package:flutter_utility/widget/value_widget.dart';

class ContainsPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final hasLamborghini = cars.contains("Lamborghini");
    return Center(
      child: hasLamborghini
          ? ValuesWidget(
              title: "Best Car ",
              values: ["Lamborghini", "Car"],
            )
          : ValuesWidget(
              title: "Best Car",
              values: [
                "None",
              ],
            ),
    );
  }
}
