import 'package:flutter/material.dart';
import 'package:flutter_utility/data/cars.dart';
import 'package:flutter_utility/widget/value_widget.dart';

class ReducePage extends StatefulWidget {
  @override
  _ReducePageState createState() => _ReducePageState();
}

class _ReducePageState extends State<ReducePage> {
  @override
  Widget build(BuildContext context) {
    final joinedcars = cars.reduce((value, element) => '$value & $element');
    return Center(
      child: ValueWidget(
        title: "Joined cars",
        value: joinedcars,
      ),
    );
  }
}
