import 'package:flutter/material.dart';
import 'package:flutter_utility/data/bikes.dart';
import 'package:flutter_utility/widget/value_widget.dart';

class WherePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bikesShortNamed = bikes.where((bike) => bike.name.length <= 5);
    final nameBikes = bikes.where((bike) => bike.name.startsWith('H'));
    final nameToUpperCase = bikes.where((bike) => bike.name.startsWith('K'));
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        const SizedBox(
          height: 10,
        ),
        ValuesWidget(
          title: "The name are:",
          values: bikesShortNamed.map((bike) => bike.name).toList(),
        ),
        const SizedBox(
          height: 10,
        ),
        ValuesWidget(
          title: "The name starts with 'H' are:",
          values: nameBikes.map((bike) => bike.name).toList(),
        ),
        const SizedBox(
          height: 10,
        ),
        ValuesWidget(
          title: "The name to uppercase:",
          values:
              nameToUpperCase.map((bike) => bike.name.toUpperCase()).toList(),
        ),
      ],
    );
  }
}
