import 'package:flutter/material.dart';
import 'package:flutter_utility/data/bikes.dart';
import 'package:flutter_utility/widget/value_widget.dart';

class TakeSkipPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final bikeStarting = bikes.take(3);
    final bikeSkip = bikes.skip(3);
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        ValuesWidget(
          title: "All",
          values: bikes.map((bike) => bike.name).toList(),
        ),
        SizedBox(
          height: 10,
        ),
        ValuesWidget(
          title: "Skip Method",
          values: bikeStarting.map((bike) => bike.name).toList(),
        ),
        SizedBox(
          height: 10,
        ),
        ValuesWidget(
          title: "Take Method",
          values: bikeSkip.map((bike) => bike.name).toList(),
        ),
      ],
    );
  }
}
