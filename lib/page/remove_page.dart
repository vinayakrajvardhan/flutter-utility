import 'package:flutter/material.dart';
import 'package:flutter_utility/data/bikes.dart';
import 'package:flutter_utility/widget/value_widget.dart';

class RemovePage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final removeBikes = bikes.removeWhere((bike) => bike.name == 'Honda');

    return Scaffold(
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Center(
            child: TextButton(
              onPressed: () => removeBikes,
              child: Text(
                "Remove",
                style: TextStyle(
                  fontSize: 23,
                  fontWeight: FontWeight.bold,
                ),
              ),
            ),
          ),
          ValuesWidget(
            title: "Show",
            values: bikes.map((bike) => bike.name).toList(),
          )
        ],
      ),
    );
  }
}
