import 'package:flutter/material.dart';
import 'package:flutter_utility/data/bikes.dart';

class MapPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final allHorsePower = bikes.map((bike) {
      final horsePower = bike.horsepower.toString();
      return Text(
        horsePower,
        style: TextStyle(
          fontSize: 32,
          fontWeight: FontWeight.bold,
          color: Colors.red,
        ),
      );
    }).toList();

    final nameHorse = bikes.map((bike) {
      final bikeName = bike.name.toString();
      return Text(
        bikeName,
        style: TextStyle(
          fontSize: 32,
          fontWeight: FontWeight.bold,
          color: Colors.teal,
        ),
      );
    }).toList();
    return Column(
      mainAxisAlignment: MainAxisAlignment.center,
      children: [
        Text(
          "All Bike'\s horsepower",
          style: TextStyle(
            fontSize: 32,
            fontWeight: FontWeight.bold,
            color: Colors.black,
          ),
        ),
        const SizedBox(
          height: 10,
        ),
        ...?allHorsePower,
        const SizedBox(
          height: 10,
        ),
        ...?nameHorse,
      ],
    );
  }
}
