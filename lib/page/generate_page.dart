import 'package:flutter/material.dart';
import 'package:flutter_utility/data/bikes.dart';
import 'package:flutter_utility/widget/value_widget.dart';

class GeneratePage extends StatelessWidget {
  final generateBike =
      List.generate(bikes.length, (index) => bikes[index].name);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
            ValuesWidget(
              title: "show cards",
              values: generateBike.map((bike) => bike.toString()).toList(),
            )
          ],
        ),
      ),
    );
  }
}
