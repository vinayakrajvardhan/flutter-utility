import 'package:flutter/material.dart';
import 'package:flutter_utility/data/bikes.dart';
import 'package:flutter_utility/widget/value_widget.dart';

class EveryPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final isHighHorsePower = bikes.every((bike) => bike.horsepower >= 350);
    return Center(
      child: ValueWidget(
        title: "Check the HorsePower",
        value: isHighHorsePower.toString(),
      ),
    );
  }
}
